<?php
/**
 * These are the database login details
 */  
define("HOST", "localhost");     		// The host you want to connect to.
define("USER", "root");    				// The database username. 
define("PASSWORD", "mysql");    		// The database password. 
define("DATABASE", "clockingSystem");   // The database name.

/* 

SQL to get all data from the databasse within the last week
SELECT * FROM `log` 
join users on log.user_id = users.id 
join event_list on event_list.id = log.event_id 
where 
date_time <= CURRENT_DATE 
and 
date_time > (NOW() - INTERVAL 1 WEEK)

*/
?>